#!/usr/bin/env python

def eixos(t, sc):
    def eixo(t, s):
        t.fd(s);t.back(s+s);t.fd(s)
    x,y = sc.screensize()
    eixo(t,x)
    t.left(90)
    eixo(t, y)
    t.right(90)

def draw_seg(t, s):
    old_color = t.pencolor()
    t.pencolor('red')
    t.penup()
    t.goto(s.x0*50, s.eval(s.x0*50))
    print(f'ini {s.x0*50} {s.eval(s.x0*50)}')
    t.pendown()
    if s.xf != None:
        t.goto(s.xf*50, s.eval(s.xf*50))
        print(f'goto {s.xf*50} {s.eval(s.xf*50)}')
    else:
        print(f'goto inf {s.eval(500)}')
        t.goto(500, s.eval(500))
    t.penup()
    t.goto(0,0)
    t.pencolor(old_color)

class click_clicker:
    def __init__(self, draw, collection, t, sc):
        self.collection = collection
        self.idx = 0
        self.draw = draw
        self.t = t
        self.sc = sc
    
    def accept_click(self, x, y):
        try:
            v = self.collection[self.idx]
            self.idx += 1
            self.sc.reset()
            t.speed(0)
            eixos(self.t, self.sc)
            self.draw(self.t, v)
        except IndexError:
            sc.bye()

class seg:
    def __init__(self, x0, xf, a_pr, b):
        self.x0 = x0
        self.xf = xf
        self.a = a_pr/100
        self.b = b

    def eval(self, x):
        return self.a*x + self.b*50

def mah_draw(t, x):
    for seg in x:
        draw_seg(t, seg)

import turtle
sc = turtle.Screen()
t = turtle.Turtle()
t.shape('blank')

seg1_1 = [seg(0, None, 100, 0)]
seg2_1 = [seg(0, 1, 100, 0), seg(1, None, 100, 1)]
seg2_2 = [seg(0, 1, 100, 0), seg(1, None, 0, 1)]
seg2_3 = [seg(0, 2, 100, 0), seg(2, None, 50, 1)]

sc.onclick(click_clicker(lambda t,x: mah_draw(t, x), [seg1_1, seg2_1, seg2_2, seg2_3], t, sc).accept_click)
sc.mainloop()
