# blog-companion

Apenas um companion para o blog, com alguns códigos para facilitar os exemplos.
Não espere muita estruturação.

## Pequenas considerações sobre os códigos

Exceto se especificado o contrário, os códigos são rodados
no Windows. Normalmente uso o VIM, Notepad++ ou alguma
ferramenta da JetBrains, mas procuro sempre ter a disposição
a capacidade de executá-la na minha máquina.

Linguagens em suas versões preferidas:

> Se algo for alterado, terá algo indicando que a versão de
> escolha foi alterado no dia ZZZZ-YY-XX, então considere que
> tudo antes dessa data foi da versão anterior

- Ruby 3.0
- Python 3.9
- Java 16
- Bash 4.4
