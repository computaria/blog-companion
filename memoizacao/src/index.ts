import { f, f_iterative, f_recursive, f_self_rec } from './memoizacao.js'
import { createMemoization, fix_point, fix_point_memoized } from './generic_memoizacao.js'
console.log(f(6))
console.log(f(6))
console.log(f(6))

const fib = (n: number) => {
  if (n == 0) {
    return 0;
  }
  if (n == 1) {
    return 1;
  }
  let fib_n_minus_2 = 0;
  let fib_n_minus_1 = 1;
  let fib_n = 1;
  for (let i = 2; i <= n; i++) {
    fib_n = fib_n_minus_1 + fib_n_minus_2;
    fib_n_minus_2 = fib_n_minus_1;
    fib_n_minus_1 = fib_n;
  }
  return fib_n;
}

const f_memoized = createMemoization(f_recursive);
const f_self = fix_point(f_self_rec);
const f_self_memo = fix_point_memoized(f_self_rec);

console.log(f_self_memo(10));

for (let i = 0; i <= 12; i++) {
  console.log({
	  i,
	  f:f(i),
	  f_iterative:f_iterative(i),
	  f_recursive:f_recursive(i),
	  f_memoized:f_memoized(i),
	  f_self: f_self(i),
	  f_self_memo: f_self_memo(i)
  });
}
