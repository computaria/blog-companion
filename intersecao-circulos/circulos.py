#!/usr/bin/env python

def eixos(t, sc):
  def eixo(t, s):
    t.fd(s);t.back(s+s);t.fd(s)
  x,y = sc.screensize()
  eixo(t,x)
  t.left(90)
  eixo(t, y)
  t.right(90)

def circulos(t, r1, r2, c2 = 0):
  def circulo(t, r, c):
    # considerando a tartaruga na origem
    t.penup()
    t.fd(r + c)
    t.pendown()
    t.left(90)
    t.circle(r)
    t.right(90)
    t.penup()
    t.back(r+c)
    t.pendown()
  circulo(t, r1, 0)
  old_color = t.pencolor()
  t.pencolor('red')
  circulo(t, r2, c2)
  t.pencolor(old_color)
  print(f'imprimiu o círculo com deslocamento {c2}')

class click_clicker:
  def __init__(self, draw, collection, t, sc):
    self.collection = collection
    self.idx = 0
    self.draw = draw
    self.t = t
    self.sc = sc
  
  def accept_click(self, x, y):
    try:
      v = self.collection[self.idx]
      self.idx += 1
      self.sc.reset()
      t.speed(0)
      eixos(self.t, self.sc)
      self.draw(self.t, v)
    except IndexError:
      sc.bye()


import turtle
sc = turtle.Screen()
t = turtle.Turtle()
t.shape('blank')

sc.onclick(click_clicker(lambda t,x: circulos(t, 100, 10, x), [0, 80, 90, 95, 100, 105, 110, 120], t, sc).accept_click)
sc.mainloop()