// comentário de linha{

/* e o
  de bloco{ */
public enum Day05 {
	SUNDAY /* um bloco *no meio */, MONDAY, TUESDAY, WEDNESDAY,
	// quebrando a linha} */;
	THURSDAY, FRIDAY, SATURDAY;
	
	// oops quebrando a linha
	private final int weekday;
	Day05() {
		weekday = -1;
	}
	Day05(int weekday) {
		this.weekday = weekday;
	}
	/* passando aqui
	   com o meu bloco */
}
