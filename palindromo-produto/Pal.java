import java.time.Instant;
import static java.time.temporal.ChronoUnit.*;

public class Pal {
	public static boolean isPalindrome(int n) {
		int r = n;
		int d1 = r % 10;
		r = (r) / 10;
		int d2 = r % 10;
		r = (r) / 10;
		int d3 = r % 10;
		r = (r) / 10;
		int d4 = r % 10;
		r = (r) / 10;
		int d5 = r % 10;
		r = (r) / 10;
		int d6 = r % 10;
		r = (r) / 10;
		return d1 == d6 && d2 == d5 && d3 == d4 && r == 0;
	}

	public static void main(String[] args) {
		Instant x1 = Instant.now();
		int greater = 0;

		for (int x = 999; x >= 100; x--) {
			for (int prod = x * x; prod > greater; prod -= x) {
				if (isPalindrome(prod)) {
					greater = prod;
					break;
				}
			}
		}

		Instant x2 = Instant.now();
		System.out.println(greater);
		System.out.print(x1.until(x2, NANOS));
	}
}