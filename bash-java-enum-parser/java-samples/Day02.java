public enum Day02 {
	SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	THURSDAY, FRIDAY, SATURDAY;
	
	private final int weekday;
	Day02() {
		weekday = -1;
	}
	Day02(int weekday) {
		this.weekday = weekday;
	}
}
