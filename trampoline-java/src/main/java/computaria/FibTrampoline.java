package computaria;

import java.util.stream.IntStream;

public class FibTrampoline {
    public static TrampolineStep<Integer> nextStep(int count, int a, int b) {
        if (count == 0) {
            return TrampolineStep.valueFound(a);
        }
        return TrampolineStep.goonStep(() -> nextStep(count - 1, b, a + b));
    }

    public static TrampolineStep<Integer> initiateFibTrampolineStep(Integer input) {
        return TrampolineStep.goonStep(() -> nextStep(input, 0, 1));
    }

    public static void main(String[] args) {
        IntStream.rangeClosed(0, 100)
            .map(i -> Trampoline.trampoline(i, FibTrampoline::initiateFibTrampolineStep))
            .forEach(System.out::println);
    }
}
