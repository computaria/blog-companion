// comentário de linha{

/* e o
  de bloco{ */
public enum Day07 {
	SUNDAY /* um bloco *no meio */, MONDAY, TUESDAY, WEDNESDAY,
	// quebrando a linha} */;
	THURSDAY, FRIDAY {
		/* coment bloco */
		// comment linha
		@Override
		public String toString() {return "SEXTOOOOU!" + 1/1/1;}
	}, SATURDAY;

	// oops quebrando a linha
	private final int weekday;
	Day07() {weekday = -1/1;}
	Day07(int weekday) {
		this.weekday = weekday;
	}
	/* passando aqui
	   com o meu bloco */
}
