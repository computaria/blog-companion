// comentário de linha

/* e o
  de bloco */
public enum Day03 {
	SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	THURSDAY, FRIDAY, SATURDAY;
	
	private final int weekday;
	Day03() {
		weekday = -1;
	}
	Day03(int weekday) {
		this.weekday = weekday;
	}
}
