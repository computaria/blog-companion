// comentário de linha{

/* e o
  de bloco{ */
public enum Day06 {
	SUNDAY /* um bloco *no meio */, MONDAY, TUESDAY, WEDNESDAY,
	// quebrando a linha} */;
	THURSDAY, FRIDAY {

		@Override
		public String toString() {
			return "SEXTOOOOU!";
		}
	}, SATURDAY;

	// oops quebrando a linha
	private final int weekday;
	Day06() {
		weekday = -1;
	}
	Day06(int weekday) {
		this.weekday = weekday;
	}
	/* passando aqui
	   com o meu bloco */
}
