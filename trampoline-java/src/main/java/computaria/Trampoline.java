package computaria;

import java.util.function.Function;

public class Trampoline<IN, R> {
    
    public static <IN, R> R trampoline(IN input, Function<IN, TrampolineStep<R>> trampolinebootStrap) {
        TrampolineStep<R> nextStep = trampolinebootStrap.apply(input);
        while (!nextStep.gotValue()) {
            nextStep = nextStep.runNextStep();
        }
        return nextStep.value();
    }
}
