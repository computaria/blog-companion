#!/bin/bash

set -euo pipefail

base_extract_dir=extract-enum/
java_sample_dir=java-samples/

EXTRACTOR="$1"
JAVA_SAMPLE="$2"

if [ "${EXTRACTOR##*/}" = "${EXTRACTOR}" ]; then
	EXTRACTOR=$base_extract_dir/$EXTRACTOR
fi

if [ ! -e "${EXTRACTOR}" ]; then
	echo "Arquivo '$EXTRACTOR' não é executável" >&2
	exit 1
fi

if [ "${JAVA_SAMPLE##*/}" = "${JAVA_SAMPLE}" ]; then
	JAVA_SAMPLE=$java_sample_dir/$JAVA_SAMPLE
fi

if [ "${JAVA_SAMPLE%.*}" = "${JAVA_SAMPLE}" ]; then
	RES=$JAVA_SAMPLE.res
	JAVA_SAMPLE=$JAVA_SAMPLE.java
else
	RES=${JAVA_SAMPLE%.*}.res
fi

if [ ! -f "${JAVA_SAMPLE}" ]; then
	echo "Arquivo '$JAVA_SAMPLE' não existe" >&2
	exit 1
fi
if [ ! -f "${RES}" ]; then
	echo "Arquivo '$RES' não existe" >&2
	exit 1
fi

"$EXTRACTOR" <"$JAVA_SAMPLE" | diff - "$RES"
