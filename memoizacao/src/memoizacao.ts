const memory: number[] = [];

export function f(x: number): number {
	if (x < 3) {
		return x;
	}
	const fromMemory = memory[x];
	if (fromMemory != undefined) {
		return fromMemory;
	}
	const newValue = f(x - 1) + 2*f(x - 2) + 3*f(x - 3);
	memory[x] = newValue;
	return newValue;
}

export const f2 = (n: number): number => {
	return f(n);
}

export const f_iterative = (n: number) => {
	if (n < 3) {
		return n;
	}
	let f_n_minus_3 = 0;
	let f_n_minus_2 = 1;
	let f_n_minus_1 = 2;
	let f_n = -1;
	for (let i = 3; i <= n; i++) {
		f_n = f_n_minus_1 + 2*f_n_minus_2 + 3*f_n_minus_3;
		f_n_minus_3 = f_n_minus_2;
		f_n_minus_2 = f_n_minus_1;
		f_n_minus_1 = f_n;
	}
	return f_n;
}

export const f_recursive = (n: number): number => n < 3? n: f(n-1) + 2*f(n-2) + 3*f(n-3);

import { RecFunc } from './generic_memoizacao.js'

export function f_self_rec(x: number, self_f: RecFunc) {
	if (x < 3) {
		return x;
	}
	return self_f(x-1, self_f) + 2*self_f(x-2, self_f) + 3*self_f(x-3, self_f);
}
