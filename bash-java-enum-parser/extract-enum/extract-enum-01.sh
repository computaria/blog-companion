#!/bin/bash

set -euo pipefail

state=INICIAL
enum_lida=""

while read -n1 CARACTER; do
	case "$state" in
		INICIAL)
			if [ "$CARACTER" = '{' ]; then
				state=POSSIVEL_ENUM
			fi
			;;
		POSSIVEL_ENUM)
			if [[ "$CARACTER" = [A-Za-z0-9_] ]]; then
				enum_lida+="$CARACTER"
			else
				if [ -n "$enum_lida" ]; then
					echo "$enum_lida"
					enum_lida=''
				fi
				if [ "$CARACTER" = '}' ] || [ "$CARACTER" = ';' ]; then
					state=EOE
				fi
			fi
			;;
		EOE)
			break
			;;
	esac
done
