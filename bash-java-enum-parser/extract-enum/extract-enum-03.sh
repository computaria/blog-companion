#!/bin/bash

set -euo pipefail

leitura_comentario_linha() {
	local CARACTER
	while read -N1 CARACTER; do
		#echo "comment linha >$CARACTER<" >&2
		if [ -z "$CARACTER" ] || [ "$CARACTER" = $'\r' ] || [ "$CARACTER" = $'\n' ]; then
			return
		fi
	done
}

leitura_comentario_bloco() {
	local CARACTER
	local star=false
	while read -N1 CARACTER; do
		if [ "$CARACTER" = '*' ]; then
			star=true
		elif $star; then
			if [ "$CARACTER" = '/' ]; then
				return
			fi
			star=false
		fi
	done
}

state=INICIAL
enum_lida=""

barra=false

while read -N1 CARACTER; do
	case "$state" in
		INICIAL)
			if [ "$CARACTER" = '{' ]; then
				state=POSSIVEL_ENUM
			fi
			;;
		POSSIVEL_ENUM)
			if [[ "$CARACTER" = [A-Za-z0-9_] ]]; then
				enum_lida+="$CARACTER"
			else
				if [ -n "$enum_lida" ]; then
					echo "$enum_lida"
					enum_lida=''
				fi
				if [ "$CARACTER" = '}' ] || [ "$CARACTER" = ';' ]; then
					state=EOE
				fi
			fi
			;;
		EOE)
			break
			;;
	esac
	if [ "$CARACTER" = / ]; then
		if $barra; then
			leitura_comentario_linha
			barra=false
		else
			barra=true
		fi
	elif $barra; then
		if [ "$CARACTER" = '*' ]; then
			leitura_comentario_bloco
		fi
		barra=false
	fi
done
