function i_palindromo(n) {
	let r = n;
	const d1 = r%10;
	r = (r-d1)/10;
	const d2 = r%10;
	r = (r-d2)/10;
	const d3 = r%10;
	r = (r-d3)/10;
	const d4 = r%10;
	r = (r-d4)/10;
	const d5 = r%10;
	r = (r-d5)/10;
	const d6 = r%10;
	r = (r-d6);
	return d1 == d6 && d2 == d5 && d3 == d4 && r == 0;
}

function s_palindromo(n) {
	const x = "" + n;
	return x == x.split("").reverse().join("")
}

function palindromo(n) {
	return i_palindromo(n)
}

function acharParada() {
	let maior = -1;
	for (let i = 999; i >= 100; i--) {
		//for (let j = i; j >= 100; j--) {
		for (let p = i*i; p >= maior; p -= i) {
		//	const p = i*j;
			if (palindromo(p)) {
				//console.log({i, j, p, maior})
			//	if(p > maior) {
					//console.log({p, maior, msg: 'p substituiu maior'})
					maior = p;
			//	}
				break;
			}
		}
	}
	return maior;
}

console.time();
//const d1 = new Date();
const p = acharParada();
//console.log(acharParada());
//const d2 = new Date();
console.timeEnd();
console.log(p);

//console.log(d2 - d1);
