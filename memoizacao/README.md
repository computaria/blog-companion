Companion do post [`memoizacao`](https://computaria.gitlab.io/blog/2023/01/03/memoizacao)

Para rodar, tenha `node` e `yarn`. Então, no diretório atual, execute os seguintes comandos:

```bash
$ yarn
$ yarn run-test
```
