package computaria;

import java.util.function.Supplier;

public interface TrampolineStep<X> {

    boolean gotValue();
    X value();
    TrampolineStep<X> runNextStep();

    static <X> TrampolineStep<X> valueFound(X value) {
        return new TrampolineStep<>() {
            @Override
            public boolean gotValue() {
                return true;
            }

            @Override
            public X value() {
                return value;
            }

            @Override
            public TrampolineStep<X> runNextStep() {
                return this;
            }
        };
    }

    static <X> TrampolineStep<X> goonStep(Supplier<TrampolineStep<X>> x) {
        return new TrampolineStep<>() {
            @Override
            public boolean gotValue() {
                return false;
            }

            @Override
            public X value() {
                throw new RuntimeException("dont call this");
            }

            @Override
            public TrampolineStep<X> runNextStep() {
                return x.get();
            }
        };
    }
}
