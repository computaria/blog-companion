export function createMemoization(f: (n:number)=>number): (n:number)=>number {
  const memory: number[] = [];
  return (n) => {
    const fromMemory = memory[n];
    if (fromMemory != undefined) {
      return fromMemory;
    }
    const newValue = f(n);
    memory[n] = newValue;
    return newValue;
  };
}

export type RecFunc = (n: number, f: RecFunc) => number;

export function fix_point(f: RecFunc): (x: number) => number {
	return x => f(x, f)
}

export function fix_point_memoized(f: RecFunc): (n: number) => number {
  const memory: number[] = [];
  const memoized_f = (n: number, self_f: RecFunc): number => {
    const fromMemory = memory[n];
    if (fromMemory != undefined) {
      return fromMemory;
    }
    const newValue = f(n, self_f);
    memory[n] = newValue;
    return newValue;
  }
  return n => memoized_f(n, memoized_f);
}
