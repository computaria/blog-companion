import java.time.Instant;
import static java.time.temporal.ChronoUnit.*;

class Domingos {
	public static void main(String... args) {
		Instant x1 = Instant.now();
		int deltaMes[] = {
			31, // jan 1
			28, // fev 1
			31, // mar 1
			30, // abr 1
			31, // mai 1
			30, // jun 1
			31, // jul 1
			31, // ago 1
			30, // set 1
			31, // out 1
			30, // nov 1
			31, // dez 1
			31, // jan 2
			28, // fev 2
			31, // mar 2
			30, // abr 2
			31, // mai 2
			30, // jun 2
			31, // jul 2
			31, // ago 2
			30, // set 2
			31, // out 2
			30, // nov 2
			31, // dez 2
			31, // jan 3
			28, // fev 3
			31, // mar 3
			30, // abr 3
			31, // mai 3
			30, // jun 3
			31, // jul 3
			31, // ago 3
			30, // set 3
			31, // out 3
			30, // nov 3
			31, // dez 3
			31, // jan 4
			29, // fev 4
			31, // mar 4
			30, // abr 4
			31, // mai 4
			30, // jun 4
			31, // jul 4
			31, // ago 4
			30, // set 4
			31, // out 4
			30, // nov 4
			31, // dez 4
		};
		for (int i = 0; i < deltaMes.length; i++) {
			deltaMes[i] = deltaMes[i] % 7;
		}

		int diaSemanaAtual = 2; // terça-feira, 01, jan, 1901
		int domingosPrimeiro = 0; // qnt de dias primeiro que são domingo
		for (int i = 0; i < 100*12 /* 100 anos, thus 100 12 meses */; i++) {
			if (diaSemanaAtual == 0) {
				domingosPrimeiro++;
			}
			final int delta = deltaMes[i % deltaMes.length];
			diaSemanaAtual = (diaSemanaAtual + delta) % 7;
		}
		Instant x2 = Instant.now();
		System.out.println(domingosPrimeiro);
		System.out.print(x1.until(x2, MILLIS));

	}
}
